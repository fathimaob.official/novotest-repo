const jwt = require("jsonwebtoken");
const SECRET = "appointmentBooking_2021";

module.exports = {
	authToken(req, res, next) {
		const bearerToken = req.header("Authorization");

		if (typeof bearerToken !== "undefined") {
			jwt.verify(bearerToken, SECRET, (err, user) => {
				if (err) {
					return res.sendStatus(401);
				} else {
					req.user = user;
					next();
				}
			});
		} else {
			res.sendStatus(401);
		}
	},
};
