const express = require("express");
const { authToken } = require("../middlewares/authToken");
const { createSlots, getAllSlots } = require("../controllers/slotController");

const routes = express.Router();

routes.post("/create-slot", authToken, createSlots);
routes.get("/get-all-slot", authToken, getAllSlots);

module.exports = routes;
