const express = require("express");
const { authToken } = require("../middlewares/authToken");
const {
	createUser,
	loginUser,
	getSellers,
} = require("../controllers/authController");

const routes = express.Router();

routes.post("/register", createUser);
routes.post("/login", loginUser);
routes.get("/sellers?", authToken, getSellers);

module.exports = routes;
