const express = require("express");
const { authToken } = require("../middlewares/authToken");

const routes = express.Router();

const {
	getAllrequestBySeller,
	createRequest,
	manageRequest,
} = require("../controllers/requestController");

routes.post("/create-request", authToken, createRequest);
routes.put("/manage-request?", authToken, manageRequest);
routes.get("/get-seller-requests", authToken, getAllrequestBySeller);

module.exports = routes;
