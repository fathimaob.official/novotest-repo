const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/User");
const Request = require("../models/Request");
const SECRET = "appointmentBooking_2021";

module.exports = {
	// Create new User
	createUser: async (req, res) => {
		try {
			const { name, password, email, role } = req.body;

			// Required Check
			if (!name || !email || !password || !role) {
				return res.status(400).json({
					isSuccess: false,
					message: "Required fields are missing",
				});
			}

			// Find if existing user
			const existingUser = await User.findOne({ email });

			if (!existingUser) {
				const hashPassword = await bcrypt.hash(password, 10);
				const user = await User.create({
					name,
					email,
					password: hashPassword,
					role,
					slots: [],
				});

				const userResponse = {
					_id: user._id,
					name: user.name,
					email: user.email,
					role: user.role,
					slots: [],
				};

				return jwt.sign(userResponse, SECRET, (err, token) =>
					res.json({
						isSuccess: true,
						token: token,
						_id: userResponse._id,
						name: userResponse.name,
						email: userResponse.email,
						role: userResponse.role,
						message: "User registered successfully!",
					})
				);
			} else
				res.status(400).json({
					isSuccess: false,
					message: "User already exist",
				});
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},

	// Login User
	loginUser: async (req, res) => {
		try {
			const { email, password } = req.body;

			if (!email || !password) {
				return res.status(400).json({
					isSuccess: false,
					message: "Required fields are missing",
				});
			}

			let user = await User.findOne({ email });

			if (!user) {
				return res.status(400).json({
					message: "User not found. Please Register instead",
				});
			}

			if (user && (await bcrypt.compare(password, user.password))) {
				const userResponse = {
					_id: user._id,
					name: user.name,
					email: user.email,
					role: user.role,
				};

				return jwt.sign(userResponse, SECRET, (err, token) =>
					res.json({
						isSuccess: true,
						token,
						_id: userResponse._id,
						name: userResponse.name,
						email: userResponse.email,
						role: userResponse.role,
					})
				);
			} else {
				return res.status(400).json({
					isSuccess: false,
					message: "Email or Password does not match",
				});
			}
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},

	// List Sellers
	getSellers: async (req, res) => {
		try {
			const { search } = req.query;
			const { _id } = req.user;
			const query = {
				name: { $regex: search, $options: "i" },
				role: "Seller",
			};

			// All requests the buyer made
			const allRequests = await Request.find({
				buyer_id: { $eq: _id },
			}).select("slot status");

			const sellers = await User.find(query)
				.select("name")
				.populate("slots")
				.exec();
			return res.send({
				isSuccess: true,
				data: sellers,
				myRequests: allRequests,
			});
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internal Server Error",
			});
		}
	},
};
