const Slot = require("../models/Slot");
const Request = require("../models/Request");

module.exports = {
	// Get Requests of a seller
	getAllrequestBySeller: async (req, res) => {
		try {
			const { _id } = req.user;
			const requests = await Request.find({ seller_id: _id, status: 0 })
				.populate("slot")
				.populate("buyer_id", "-password")
				.exec();
			return res.send({
				isSuccess: true,
				data: requests,
			});
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},

	// Create Requests
	createRequest: async (req, res) => {
		try {
			const { slot_id } = req.body;
			const { _id } = req.user;

			// Find the slot from slots list
			const slot = await Slot.findById(slot_id);

			// Find if there is already same slot from same buyer
			const request = await Request.findOne({
				slot: slot_id,
				buyer_id: _id,
			});

			if (slot && !slot.isBooked && !request) {
				const newRrequest = await Request.create({
					seller_id: slot.seller_id,
					buyer_id: _id,
					slot: slot_id,
				});
				return res.send({
					isSuccess: true,
					data: newRrequest,
					message: "Slot Requested",
				});
			} else {
				return res.status(400).json({
					isSuccess: false,
					message: "Slot Unavailable",
				});
			}
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},

	// Accept / Decline request
	manageRequest: async (req, res) => {
		try {
			const { accept, reject, buyer_id } = req.query;
			const slot = accept
				? await Slot.findById(accept)
				: await Slot.findById(reject);

			if (accept && !slot.isBooked) {
				await Slot.updateOne({ _id: accept }, { isBooked: true });
				await Request.updateOne(
					{ slot: accept, buyer_id },
					{ status: 1 }
				);
				return res.status(200).json({
					isSuccess: true,
					message: "Slot Accepted",
				});
			} else if (reject) {
				await Request.updateOne(
					{ slot: reject, buyer_id },
					{ status: 2 }
				);
				return res.status(200).json({
					isSuccess: true,
					message: "Slot Rejected",
				});
			} else {
				return res.status(400).json({
					isSuccess: false,
					message: "Slot already booked",
				});
			}
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},
};
