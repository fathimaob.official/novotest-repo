const User = require("../models/User");
const Slot = require("../models/Slot");

module.exports = {
	// Get Slots of a seller
	getAllSlots: async (req, res) => {
		try {
			const { _id } = req.user;
			const requests = await Slot.find({ seller_id: _id });
			return res.send({
				isSuccess: true,
				data: requests,
			});
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},

	// Create Slots
	createSlots: async (req, res) => {
		try {
			const { _id } = req.user;
			const { name, time } = req.body;

			const seller_id = _id;

			const existingSlot = await Slot.findOne({ seller_id, time });
			if (!existingSlot) {
				const newSlot = await Slot.create({
					name,
					time,
					seller_id,
				});
				await User.updateOne(
					{ _id: seller_id },
					{ $push: { slots: newSlot._id } }
				);
				return res.send({
					isSuccess: true,
					data: newSlot,
				});
			} else {
				return res.status(400).json({
					isSuccess: false,
					message: "Slot unavailable",
				});
			}
		} catch (error) {
			return res.status(500).json({
				isSuccess: false,
				message: "Internel Server Error",
			});
		}
	},
};
