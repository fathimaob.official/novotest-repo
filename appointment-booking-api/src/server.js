const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const authRoutes = require("./routes/authRoutes");
const slotRoutes = require("./routes/slotRoutes");
const requestRoutes = require("./routes/requestRoutes");

const app = express();

const PORT = 8000;
const MONGO_DB_CONNECTION =
	"mongodb+srv://fathima_novo_labs:test@novolabstest.e86eb.mongodb.net/novoLabsTest?retryWrites=true&w=majority";

//DB connection
try {
	mongoose.connect(MONGO_DB_CONNECTION, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	console.log(`DB connected!`);
} catch (error) {
	console.log(error);
}

app.use(cors());
app.use(express.json());

app.use("/auth", authRoutes);
app.use("/slot", slotRoutes);
app.use("/request", requestRoutes);

app.listen(PORT, () => {
	console.log(`App listening on port ${PORT}!`);
});
