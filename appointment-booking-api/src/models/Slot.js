const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const slotSchema = new Schema({
	name: {
		type: String,
		required: true,
	},
	time: {
		type: String,
		required: true,
	},
	seller_id: {
		type: Schema.Types.ObjectId,
		ref: "User",
	},
	isBooked: {
		type: Boolean,
		default: false,
	},
});

module.exports = mongoose.model("Slot", slotSchema);
