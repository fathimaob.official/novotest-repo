const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
	name: {
		type: String,
		required: true,
	},
	role: {
		type: String,
		required: true,
	},
	password: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
	},
	slots: [
		{
			type: Schema.Types.ObjectId,
			ref: "Slot",
		},
	],
	requests: [
		{
			type: Schema.Types.ObjectId,
			ref: "Request",
		},
	],
});

module.exports = mongoose.model("User", userSchema);
