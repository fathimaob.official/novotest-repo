const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const requestSchema = new Schema({
	seller_id: {
		type: Schema.Types.ObjectId,
		ref: "User",
	},
	slot: {
		type: Schema.Types.ObjectId,
		ref: "Slot",
	},
	status: {
		type: Number,
		default: 0,
	},
	buyer_id: {
		type: Schema.Types.ObjectId,
		ref: "User",
	},
});

module.exports = mongoose.model("Request", requestSchema);
