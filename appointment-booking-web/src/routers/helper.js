import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isAuth, getUser } from "../util/util";

export const PrivateRoute = ({ component: Component, ...rest }) => {
	const isLoggedIn = () => {
		const token = isAuth();
		const currentRole = getUser()?.role;
		const isLogged = token ? true : false;
		const role_check = currentRole === rest?.role;
		return isLogged && role_check ? true : false;
	};

	return (
		<Route
			{...rest}
			render={(props) =>
				isLoggedIn() ? (
					<Component {...props} />
				) : (
					<Redirect
						to={{
							pathname: "/",
							state: { from: props.location },
						}}
					/>
				)
			}
		/>
	);
};

export default PrivateRoute;
