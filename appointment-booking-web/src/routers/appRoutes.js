import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "../pages/home";
import Slots from "../pages/slots";
import Requests from "../pages/requests";
import PrivateRoute from "./helper";
import Sellers from "../pages/sellers";

const AppRoutes = () => (
	<Router>
		<Switch>
			<Route exact path="/" component={Home} />
			<PrivateRoute exact path="/sellers" component={Sellers} role="Buyer" />
			<PrivateRoute exact path="/slots" component={Slots} role="Seller" />
			<PrivateRoute exact path="/requests" component={Requests} role="Seller" />
			<Redirect to="/signin" />
		</Switch>
	</Router>
);
export default AppRoutes;
