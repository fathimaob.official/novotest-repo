import api from "../services";
import { SELLER } from "../types";

// Get all sellers
export const getAllSellers = (search = "", successHandler, errorHandler) => {
	return api(
		{
			url: `auth/sellers?search=${search}`,
			method: "GET",
			actionType: SELLER.GET_ALL_SELLERS,
		},
		successHandler,
		errorHandler
	);
};

// ---------------- SLOTS ---------------- //

// Get all slots
export const getAllSlots = (successHandler, errorHandler) => {
	return api(
		{
			url: `slot/get-all-slot`,
			method: "GET",
			actionType: SELLER.GET_ALL_SLOTS,
		},
		successHandler,
		errorHandler
	);
};

// Create new slot
export const createNewSlot = (values, successHandler, errorHandler) => {
	return api(
		{
			url: `slot/create-slot`,
			method: "POST",
			body: values,
			actionType: SELLER.CREATE_NEW_SLOT,
		},
		successHandler,
		errorHandler
	);
};

// ---------------- REQUESTS ---------------- //

// Get all Requests
export const getAllRequests = (successHandler, errorHandler) => {
	return api(
		{
			url: `request/get-seller-requests`,
			method: "GET",
			actionType: SELLER.GET_ALL_REQUEST,
		},
		successHandler,
		errorHandler
	);
};

// Create new Requests
export const createNewRequest = (values, successHandler, errorHandler) => {
	return api(
		{
			url: `request/create-request`,
			method: "POST",
			body: values,
			actionType: SELLER.CREATE_NEW_REQUEST,
		},
		successHandler,
		errorHandler
	);
};

// Accept/Decline Requests
export const manageAllRequests = (slotId, buyerId, isAccept, successHandler, errorHandler) => {
	return api(
		{
			url: `request/manage-request?buyer_id=${buyerId}&${isAccept ? `accept=${slotId}` : `reject=${slotId}`}`,
			method: "PUT",
			actionType: SELLER.MANAGE_ALL_REQUEST,
		},
		successHandler,
		errorHandler
	);
};

// Clear States
export const clearStates = () => {
	return (dispatch, getState) => {
		dispatch({
			type: SELLER.CLEAR_STATES,
		});
	};
};
