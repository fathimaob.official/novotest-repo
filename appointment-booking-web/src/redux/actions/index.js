import { login, register } from "./authActions";
import {
	getAllSellers,
	createNewSlot,
	getAllSlots,
	getAllRequests,
	createNewRequest,
	manageAllRequests,
	clearStates,
} from "./sellerActions";

export {
	login,
	register,
	getAllSellers,
	createNewSlot,
	getAllSlots,
	getAllRequests,
	createNewRequest,
	manageAllRequests,
	clearStates,
};
