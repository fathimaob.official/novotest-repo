import api from "../services";
import { AUTH } from "../types";

export const login = (values, successHandler, errorHandler) => {
	return api(
		{
			url: `auth/login`,
			method: "POST",
			body: values,
			secure: false,
			actionType: AUTH.LOGIN,
		},
		successHandler,
		errorHandler
	);
};

export const register = (values, successHandler, errorHandler) => {
	return api(
		{
			url: `auth/register`,
			method: "POST",
			body: values,
			secure: false,
			actionType: AUTH.REGISTER,
		},
		successHandler,
		errorHandler
	);
};
