/**
 * Axios API Handler
 * @param {object} { url, method, body, secure }
 * @param {function} successHandler Pass success handling function
 * @param {function} errorHandler Pass error handling function
 */
export default function api(config, successHandler = null, errorHandler = null) {
	const { url, method = "GET", actionType, headers, body, secure = true, extraParams = {} } = config;

	return (dispatch, getState) => {
		const triggerSuccessHandler = (response) => {
			dispatch({
				type: actionType,
				payload: response,
				extraParams,
			});
			return successHandler ? successHandler(response) : null;
		};

		const LSString = localStorage.getItem("token");

		// URL
		const BASE_WEB_URL = "http://localhost:8000/";
		const apiUrl = `${BASE_WEB_URL}${url}`;

		let headersData = {
			...headers,
			Accept: "application/json",
			"Content-Type": "application/json",
		};

		if (secure) {
			headersData = {
				...headersData,
				Authorization: LSString,
			};
		}

		const req = JSON.stringify(body);

		const request = fetch(apiUrl, {
			method,
			headers: {
				...headersData,
			},
			body: req,
		});

		let status = null;
		request
			.then((data) => {
				status = data.status;
				return data.json();
			})
			.then((response) => {
				if (status && status > 399) {
					return errorHandler ? errorHandler(response) : null;
				} else {
					return triggerSuccessHandler(response);
				}
			})
			.catch((err) => {
				const errorObj = {
					error: {
						url: apiUrl,
						code: "FETCH_FAILED",
						message: err,
					},
				};
				return errorHandler ? errorHandler(errorObj) : null;
			});

		return {
			type: actionType,
			payload: request,
		};
	};
}
