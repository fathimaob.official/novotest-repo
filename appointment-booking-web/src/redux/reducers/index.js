import { combineReducers } from "redux";

import Auth from "./authReducer";
import Seller from "./sellerReducer";

export default combineReducers({ Auth, Seller });
