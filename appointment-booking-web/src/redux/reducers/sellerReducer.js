import { SELLER } from "../types";

const initialState = {
	sellersList: [],
	slotsList: [],
	requestsList: [],
	buyerRequests: [],
};

const sellerReducer = (state = initialState, action) => {
	switch (action.type) {
		case SELLER.GET_ALL_SELLERS:
			return {
				...state,
				sellersList: action?.payload?.data || [],
				buyerRequests: action?.payload?.myRequests || [],
			};
		case SELLER.GET_ALL_SLOTS:
			return {
				...state,
				slotsList: action?.payload?.data || [],
			};
		case SELLER.GET_ALL_REQUEST:
			return {
				...state,
				requestsList: action?.payload?.data || [],
			};
		case SELLER.CLEAR_STATES:
			return initialState;
		default:
			return state;
	}
};
export default sellerReducer;
