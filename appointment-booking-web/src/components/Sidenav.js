import React from "react";
import "./styles.scss";
import { Link, useHistory } from "react-router-dom";
import { clearLS, getUser } from "../util/util";
import { useDispatch } from "react-redux";
import { clearStates } from "../redux/actions";

const SideNav = () => {
	const dispatch = useDispatch()
	const history = useHistory();
	const { pathname = "" } = history?.location || {};

	const sellerNav = [
		{ route: "/slots", label: "My Slots" },
		{ route: "/requests", label: "My Requests" },
	];

	const buyerNav = [{ route: "/sellers", label: "Sellers" }];

	const userDetails = getUser();
	const currentRole = getUser().role;
	const getNav = currentRole === "Seller" ? sellerNav : currentRole === "Buyer" ? buyerNav : [];

	// Logout
	const logout = () => {
		clearLS();
		dispatch(clearStates());
		history.push("/");
	};

	return (
		<div className="sidenav-wrapper">
			<h3 className="sidenav-title">
				Appointment Booking<span>{userDetails.role} Interface</span>
			</h3>

			{getNav.map((item, index) => (
				<Link key={index} to={item.route} className={`sidenav-items ${pathname === item.route ? "active" : ""}`}>
					{item.label}
				</Link>
			))}
			<p className="sidenav-items" onClick={logout}>
				Logout
			</p>
		</div>
	);
};

export default SideNav;
