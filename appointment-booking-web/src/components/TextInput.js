import React from "react";
import { TextField } from "@material-ui/core";

const TextInput = (props) => (
	<TextField
		inputProps={{
			maxlength: 50,
			style: { color: "#0B2343" },
		}}
		InputLabelProps={{ shrink: true }}
		{...props}
	/>
);

export default TextInput;
