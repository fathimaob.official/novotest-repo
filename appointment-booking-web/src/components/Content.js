import React from "react";
import Sidenav from "./Sidenav";
import "./styles.scss";
import { useHistory } from "react-router-dom";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { getUser } from "../util/util";
import { Tooltip } from "@material-ui/core";

const Content = ({ children }) => {
	const history = useHistory();

	const { pathname = "" } = history?.location || {};
	const userDetails = getUser();

	const renderTitle = () => {
		switch (pathname) {
			case "/slots":
				return "My Slots";
			case "/requests":
				return "My Requests";
			case "/sellers":
				return "Sellers";
			default:
				return "Appointment Booking";
		}
	};

	const renderProfile = () => (
		<Tooltip title={userDetails.role}>
			<div className="profile-wrapper">
				<div>
					<p className="profile-name">{userDetails.name}</p>
					<p className="profile-email">{userDetails.email}</p>
				</div>
				<AccountCircleIcon className="profile-icon" />
			</div>
		</Tooltip>
	);

	return (
		<div className="content-wrapper">
			<Sidenav />
			<div className="content-wrapper-body">
				<div className="content-head-wrap">
					{renderTitle()}
					{renderProfile()}
				</div>
				<div className="content-wrapper-children">{children}</div>
			</div>
		</div>
	);
};

export default Content;
