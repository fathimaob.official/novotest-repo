import { toast } from "react-toastify";

export const Error = (msg = null) =>
	toast.error(msg || "Some error occured", {
		position: "top-right",
		autoClose: 5000,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: true,
		progress: undefined,
		hideProgressBar: true,
	});

export const Success = (msg = null) =>
	toast.success(msg || "Success!", {
		position: "top-right",
		autoClose: 5000,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: true,
		progress: undefined,
		hideProgressBar: true,
	});
