import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "lodash";
import { Content, Error, Success } from "../../components";
import { getAllSlots, createNewSlot } from "../../redux/actions/sellerActions";
import { AddButtonAccord, SlotCard } from "./components";
import { Grid, Divider } from "@material-ui/core";
import { isValidSlotForm } from "./helper";

const Slots = () => {
	const dispatch = useDispatch();
	const { slotsList } = useSelector((state) => state.Seller);

	const [isLoading, setIsLoading] = useState(false);

	// List all slots
	const getSellerSlots = () => {
		const success = (e) => {
			setIsLoading(false);
		};
		const error = (e) => {
			setIsLoading(false);
			Error(e?.message);
		};
		setIsLoading(true);
		dispatch(getAllSlots(success, error));
	};

	// Create new slot
	const onSubmitNewSlot = (values, resetForm) => {
		if (isValidSlotForm(values)) {
			const success = (e) => {
				setIsLoading(false);
				Success("Successfully created a slot");
				resetForm();
				getSellerSlots();
			};
			const error = (e) => {
				setIsLoading(false);
				Error(e?.message);
			};
			setIsLoading(true);
			dispatch(createNewSlot(values, success, error));
		}
	};

	useEffect(getSellerSlots, [dispatch]);

	return (
		<Content>
			<AddButtonAccord onSubmitNewSlot={onSubmitNewSlot} isLoading={isLoading} />
			<Divider />
			<Grid container className="slots-list-wrapper">
				{!isEmpty(slotsList) ? (
					slotsList && slotsList.map((item, index) => <SlotCard data={item} key={index} />)
				) : (
					<p>No results found.</p>
				)}
			</Grid>
		</Content>
	);
};

export default Slots;
