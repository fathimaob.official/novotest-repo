import {
	Card,
	CardContent,
	Tooltip,
	Button,
	Accordion,
	AccordionSummary,
	AccordionDetails,
	CircularProgress,
} from "@material-ui/core";
import EventIcon from "@material-ui/icons/Event";
import ScheduleIcon from "@material-ui/icons/Schedule";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import { Formik, Form, Field } from "formik";
import moment from "moment";
import { addSlotInitialValues, addSlotFormFields } from "./helper";

export const SlotCard = ({ data = {} }) => (
	<Card className="slot-card-wrapper">
		<CardContent className="slot-card-content">
			<p className="slot-card-title">
				{data.name || "Slot"}
				{data.isBooked ? (
					<Tooltip title="Slot Booked" arrow placement="top">
						<BookmarkIcon className="booked-icon" />
					</Tooltip>
				) : null}
			</p>
			<div className="slot-card-details-wrap">
				<p className="slot-card-detail">
					<EventIcon />
					{moment(data.time).format("DD MMMM") || "Date"}
				</p>
				<p className="slot-card-detail">
					<ScheduleIcon />
					{moment(data.time).format("h:mm a") || "Time"}
				</p>
			</div>
		</CardContent>
		{data.isBooked ? <div className="slot-card-overlapper"></div> : null}
	</Card>
);

export const AddButtonAccord = ({ isLoading, onSubmitNewSlot }) => {
	return (
		<Accordion className="accordion-add">
			<AccordionSummary className="accordion-add-summary">
				<Button className="btn btn-home btn-add">Add New Slot</Button>
			</AccordionSummary>
			<AccordionDetails className="accordion-add-details">
				{/* Form */}
				<Formik
					className="login-form-wrap"
					initialValues={addSlotInitialValues}
					onSubmit={(values, { resetForm }) => onSubmitNewSlot(values, resetForm)}>
					{({ handleChange, handleBlur, values }) => (
						<Form className="login-form">
							{addSlotFormFields(handleChange, handleBlur, values).map((item, index) => (
								<Field key={index} className="login-form-input" {...item} />
							))}
							<Button disabled={isLoading} className="btn btn-home" variant="contained" type="submit">
								{isLoading ? (
									<>
										<CircularProgress />
										Please wait
									</>
								) : (
									"Submit"
								)}
							</Button>
						</Form>
					)}
				</Formik>
			</AccordionDetails>
		</Accordion>
	);
};
