import { TextInput, Error } from "../../components";
import moment from "moment";

export const addSlotInitialValues = {
	name: "",
	time: "",
};

export const addSlotFormFields = (handleChange, handleBlur, values) => [
	{
		component: TextInput,
		type: "text",
		id: "name",
		label: "Slot Name",
		name: "name",
		value: values.name,
		onChange: handleChange,
		onBlur: handleBlur,
	},
	{
		component: TextInput,
		type: "datetime-local",
		id: "time",
		label: "Date & Time",
		name: "time",
		value: values.time,
		onChange: handleChange,
		onBlur: handleBlur,
	},
];

export const isValidSlotForm = (values) => {
	if (!values.name) {
		Error("Please enter slot name");
		return false;
	}
	if (!values.time) {
		Error("Please enter Date & Time");
		return false;
	}
	if (!moment(values.time).isAfter(new Date())) {
		Error("Please enter an upcoming Date & Time");
		return false;
	}
	return true;
};
