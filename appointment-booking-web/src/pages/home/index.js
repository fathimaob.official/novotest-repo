import React, { useState } from "react";
import "./homeStyles.scss";
import { Register, Login } from "../auth";

const Home = () => {
	const [isShowRegister, setIsShowRegister] = useState(false);
	return (
		<div className="home-wrapper">
			<h1 className="home-title">Appointment Booking</h1>
			{isShowRegister ? <Register doShowRegister={setIsShowRegister} /> : <Login doShowRegister={setIsShowRegister} />}
		</div>
	);
};

export default Home;
