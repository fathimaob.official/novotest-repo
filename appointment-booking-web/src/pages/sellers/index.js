import React, { useState, useEffect } from "react";
import { Content, Error, Success } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, size } from "lodash";
import { getAllSellers, createNewRequest } from "../../redux/actions";
import {
	Accordion,
	AccordionSummary,
	AccordionDetails,
	Grid,
	TextField,
	InputAdornment,
	Badge,
	Tooltip,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import SearchIcon from "@material-ui/icons/Search";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import EventBusyIcon from "@material-ui/icons/EventBusy";
import { SlotTable } from "./helper";

const Sellers = () => {
	const dispatch = useDispatch();
	const { sellersList } = useSelector((state) => state.Seller);

	const [search, setSearch] = useState("");
	const [isLoading, setIsLoading] = useState(false);
	const [expanded, setExpanded] = useState(false);

	// Handle expansion
	const handleChange = (panel) => (event, isExpanded) => {
		setExpanded(isExpanded ? panel : false);
	};

	// List all sellers
	const getSellers = () => {
		const success = (e) => {
			setIsLoading(false);
		};
		const error = (e) => {
			setIsLoading(false);
			Error(e?.message);
		};
		setIsLoading(true);
		dispatch(getAllSellers(search, success, error));
	};

	// Search
	const handleSearch = (e) => {
		setSearch(e?.target.value || "");
	};

	// Send appointment req
	const handleSendReq = (row) => {
		const success = (e) => {
			setIsLoading(false);
			getSellers();
			e.isSuccess ? Success(e.message) : Error(e.message);
		};
		const error = (e) => {
			setIsLoading(false);
			Error(e?.message);
		};
		setIsLoading(true);
		const body = {
			slot_id: row._id,
		};
		dispatch(createNewRequest(body, success, error));
	};

	useEffect(getSellers, [dispatch, search]);

	return (
		<Content>
			<Grid className="sellers-search-wrapper">
				<TextField
					className="sellers-search"
					InputProps={{
						startAdornment: (
							<InputAdornment position="start">
								<SearchIcon />
							</InputAdornment>
						),
					}}
					placeholder="Search by seller name"
					variant="outlined"
					onChange={handleSearch}
				/>
			</Grid>
			<Grid>
				{!isEmpty(sellersList)
					? sellersList.map((item, index) => (
							<Accordion key={index} expanded={expanded === index} onChange={handleChange(index)}>
								<AccordionSummary expandIcon={<ExpandMoreIcon />}>
									<span className="seller-name">{item.name}</span>
									<span className="seller-icon">
										{!isEmpty(item.slots) ? (
											<Tooltip title={`${size(item.slots)} slots`} placement="top">
												<Badge badgeContent={size(item.slots)} color="secondary">
													<EventAvailableIcon />
												</Badge>
											</Tooltip>
										) : (
											<Tooltip title="No slots" placement="top">
												<EventBusyIcon />
											</Tooltip>
										)}
									</span>
								</AccordionSummary>
								<AccordionDetails>
									{!isEmpty(item.slots) ? (
										<SlotTable slots={item.slots} isLoading={isLoading} handleSendReq={handleSendReq} />
									) : (
										<span className="seller-text">No slots found</span>
									)}
								</AccordionDetails>
							</Accordion>
					  ))
					: "No results found."}
			</Grid>
		</Content>
	);
};

export default Sellers;
