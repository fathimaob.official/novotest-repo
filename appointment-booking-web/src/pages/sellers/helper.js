import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";
import moment from "moment";
import { find } from "lodash";
import { useSelector } from "react-redux";

export const SlotTable = ({ slots = [], isLoading = false, handleSendReq }) => {
	const { buyerRequests = [] } = useSelector((state) => state.Seller);
	const renderStatus = (row) => {
		const obj = find(buyerRequests, ["slot", row._id]) || "";

		const isMyRequest = () => (find(buyerRequests, ["slot", row._id]) ? true : false);
		// past request
		if (moment(row.time).isBefore(new Date())) {
			return <div className="btn-status yellow">Expired</div>;
		}
		// Already booked / Not my Request
		// Already booked / My request / in requested state
		else if ((row.isBooked && !isMyRequest(row._id)) || (row.isBooked && isMyRequest(row._id) && obj.status === 0)) {
			return <div className="btn-status yellow">Unavailable</div>;
		} else {
			switch (obj.status) {
				case 0:
					return <div className="btn-status blue">Requested</div>;
				case 1:
					return <div className="btn-status green">Accepted</div>;
				case 2:
					return <div className="btn-status red">Rejected</div>;
				default:
					return (
						<Button className="btn btn-home btn-req" disabled={isLoading} onClick={() => handleSendReq(row)}>
							Send Request
						</Button>
					);
			}
		}
	};
	return (
		<TableContainer component={Paper}>
			<Table className="slotTable" aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell className="slotTable-head">Slot Name</TableCell>
						<TableCell className="slotTable-head" align="right">
							Slot Date
						</TableCell>
						<TableCell className="slotTable-head" align="right">
							Slot Time
						</TableCell>
						<TableCell className="slotTable-head" align="right">
							Appointment Request
						</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{slots.map((row, index) => (
						<TableRow key={index}>
							<TableCell component="th" scope="row">
								{row.name}
							</TableCell>
							<TableCell align="right">{moment(row.time).format("DD MMMM") || "Date"}</TableCell>
							<TableCell align="right">{moment(row.time).format("h:mm a") || "Time"}</TableCell>
							<TableCell align="right">{renderStatus(row)}</TableCell>
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};
