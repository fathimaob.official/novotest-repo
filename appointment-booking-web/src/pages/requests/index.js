import React, { useState, useEffect } from "react";
import { Content, Error, Success } from "../../components";
import { Card, CardContent, CardActions, Button, Grid } from "@material-ui/core";
import { isEmpty } from "lodash";
import EventIcon from "@material-ui/icons/Event";
import ScheduleIcon from "@material-ui/icons/Schedule";
import { useDispatch, useSelector } from "react-redux";
import { getAllRequests, manageAllRequests } from "../../redux/actions";
import { getUser } from "../../util/util";
import moment from "moment";

const Requests = () => {
	const dispatch = useDispatch();
	const { requestsList } = useSelector((state) => state.Seller);
	const [isLoading, setIsLoading] = useState(false);

	// List request
	const getSellerReq = () => {
		if (getUser()?.id) {
			const success = (e) => setIsLoading(false);
			const error = (e) => {
				setIsLoading(false);
				Error(e?.message);
			};
			setIsLoading(true);
			dispatch(getAllRequests(success, error));
		}
	};

	// Manage request
	const manageReq = (slotId, buyerId, isAccept = null) => {
		if (getUser()?.id) {
			const success = (e) => {
				getSellerReq();
				setIsLoading(false);
				e.isSuccess ? Success(e.message) : Error(e.message);
			};
			const error = (e) => {
				setIsLoading(false);
				Error(e?.message);
			};
			setIsLoading(true);
			dispatch(manageAllRequests(slotId, buyerId, isAccept, success, error));
		}
	};

	useEffect(getSellerReq, [dispatch]);

	const SlotCard = ({ data = {} }) => (
		<Card className="slot-card-wrapper">
			<CardContent className="slot-card-content">
				<p className="slot-card-title">From : {data.buyer_id.name || "Anonymous"}</p>
				<p className="slot-card-title">Slot : {data.slot.name || "Slot"}</p>
				<div className="slot-card-details-wrap">
					<p className="slot-card-detail">
						<EventIcon />
						{moment(data.slot.time).format("DD MMMM") || "Date"}
					</p>
					<p className="slot-card-detail">
						<ScheduleIcon />
						{moment(data.slot.time).format("h:mm a") || "Time"}
					</p>
				</div>
			</CardContent>
			<CardActions className="slot-card-actions">
				<Button
					variant="contained"
					className="btn btn-accept"
					onClick={() => manageReq(data.slot._id, data.buyer_id._id, true)}>
					Accept
				</Button>
				<Button
					variant="contained"
					className="btn btn-decline"
					onClick={() => manageReq(data.slot._id, data.buyer_id._id, false)}>
					Decline
				</Button>
			</CardActions>
		</Card>
	);
	return (
		<Content>
			<Grid container>
				{isLoading
					? "Loading..."
					: !isEmpty(requestsList)
					? requestsList.map((item, index) => <SlotCard data={item} key={index} />)
					: "No results found."}
			</Grid>
		</Content>
	);
};

export default Requests;
