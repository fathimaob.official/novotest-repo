import React, { useState, useEffect } from "react";
import { Formik, Form, Field } from "formik";
import { Button, CircularProgress, Checkbox, FormControlLabel } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { Error } from "../../components";
import { registerFormFields, registerInitialValues, validRegisterForm } from "./helper";
import { register, clearStates } from "../../redux/actions";
import "./authStyles.scss";
import { isAuth, clearLS, setUser } from "../../util/util";

const Register = ({ doShowRegister }) => {
	const history = useHistory();
	const dispatch = useDispatch();

	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		clearLS();
		dispatch(clearStates());
	}, []);

	// Ṛegister
	const onRegister = (values) => {
		if (validRegisterForm(values)) {
			setIsLoading(true);
			const success = (e) => {
				setUser(e);
				setIsLoading(false);
				if (isAuth()) {
					e.role === "Seller" ? history.push("/slots") : history.push("/sellers");
				}
			};
			const error = (e) => {
				setIsLoading(false);
				Error(e?.message);
				clearLS();
			};
			const body = {
				...values,
				role: values.isSeller ? "Seller" : "Buyer",
			};
			dispatch(register(body, success, error));
		}
	};

	return (
		<div className="home-content">
			{/* Title */}
			<h2 className="home-content-title">Register</h2>

			{/* Form */}
			<Formik className="login-form-wrap" initialValues={registerInitialValues} onSubmit={(values) => onRegister(values)}>
				{({ handleChange, handleBlur, values }) => (
					<Form className="login-form">
						{registerFormFields(handleChange, handleBlur, values).map((item, index) => (
							<Field key={index} className="login-form-input" {...item} />
						))}
						<FormControlLabel
							className="login-form-checkbox"
							control={<Checkbox color="primary" />}
							label="Register as seller"
							id="isSeller"
							name="isSeller"
							value={values.isSeller}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
						<Button disabled={isLoading} className="btn btn-home" variant="contained" type="submit">
							{isLoading ? (
								<>
									<CircularProgress />
									Registering...
								</>
							) : (
								"Register"
							)}
						</Button>
					</Form>
				)}
			</Formik>

			{/* Footer */}
			<p className="home-footer">
				Already have an account ?{" "}
				<span className="home-footer-link" onClick={() => doShowRegister(false)}>
					Login
				</span>
			</p>
		</div>
	);
};

export default Register;
