import React, { useState, useEffect } from "react";
import { Formik, Form, Field } from "formik";
import { Button, CircularProgress } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { loginInitialValues, loginFormFields, validLoginForm } from "./helper";
import { login, clearStates } from "../../redux/actions";
import { Error } from "../../components";
import "./authStyles.scss";
import { isAuth, clearLS, setUser } from "../../util/util";

const Login = ({ doShowRegister }) => {
	const history = useHistory();
	const dispatch = useDispatch();

	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		clearLS();
		dispatch(clearStates());
	}, [dispatch]);

	// Login
	const onLogin = (values) => {
		if (validLoginForm(values)) {
			const success = (e) => {
				setUser(e);
				setIsLoading(false);
				if (isAuth()) {
					e.role === "Seller" ? history.push("/slots") : history.push("/sellers");
				}
			};
			const error = (e) => {
				setIsLoading(false);
				Error(e?.message);
				clearLS();
			};
			setIsLoading(true);
			dispatch(login(values, success, error));
		}
	};

	return (
		<div className="home-content">
			{/* Title */}
			<h2 className="home-content-title">Sign In</h2>

			{/* Form */}
			<Formik className="login-form-wrap" initialValues={loginInitialValues} onSubmit={(values) => onLogin(values)}>
				{({ handleChange, handleBlur, values }) => (
					<Form className="login-form">
						{loginFormFields(handleChange, handleBlur, values).map((item, index) => (
							<Field key={index} className="login-form-input" {...item} />
						))}
						<Button disabled={isLoading} className="btn btn-home" variant="contained" type="submit">
							{isLoading ? (
								<>
									<CircularProgress />
									Logging in...
								</>
							) : (
								"Login"
							)}
						</Button>
					</Form>
				)}
			</Formik>

			{/* Footer */}
			<p className="home-footer">
				Don't have an account ?{" "}
				<span className="home-footer-link" onClick={() => doShowRegister(true)}>
					Register
				</span>
			</p>
		</div>
	);
};

export default Login;
