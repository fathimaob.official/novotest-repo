import { TextInput, Error } from "../../components";

export const loginInitialValues = {
	email: "",
	password: "",
};

export const loginFormFields = (handleChange, handleBlur, values) => [
	{
		component: TextInput,
		type: "text",
		id: "email",
		label: "Email",
		name: "email",
		value: values.email,
		onChange: handleChange,
		onBlur: handleBlur,
	},
	{
		component: TextInput,
		type: "password",
		id: "password",
		label: "Password",
		name: "password",
		value: values.password,
		onChange: handleChange,
		onBlur: handleBlur,
	},
];

// Login form validation
export const validLoginForm = (values) => {
	if (!values.email) {
		Error("Please enter email");
		return false;
	}
	if (!values.password) {
		Error("Please enter password");
		return false;
	}
	return true;
};

export const registerInitialValues = {
	name: "",
	email: "",
	password: "",
};

export const registerFormFields = (handleChange, handleBlur, values) => [
	{
		component: TextInput,
		type: "text",
		id: "name",
		label: "Name",
		name: "name",
		value: values.name,
		onChange: handleChange,
		onBlur: handleBlur,
	},
	{
		component: TextInput,
		type: "text",
		id: "email",
		label: "Email",
		name: "email",
		value: values.email,
		onChange: handleChange,
		onBlur: handleBlur,
	},
	{
		component: TextInput,
		type: "password",
		id: "password",
		label: "Password",
		name: "password",
		value: values.password,
		onChange: handleChange,
		onBlur: handleBlur,
	},
];

export const validRegisterForm = (values) => {
	if (!values.name) {
		Error("Please enter name");
		return false;
	}
	if (!values.email) {
		Error("Please enter email");
		return false;
	}
	if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		Error("Please enter a valid email address");
		return false;
	}
	if (!values.password) {
		Error("Please enter password");
		return false;
	}
	return true;
};
