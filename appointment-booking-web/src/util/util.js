// Store token and details
export const setUser = (data) => {
	localStorage.setItem("token", data.token);
	const resp = {
		id: data._id,
		name: data.name,
		email: data.email,
		role: data.role,
	};
	localStorage.setItem("NOVO_USER", JSON.stringify(resp));
};

// User Details
export const getUser = () => JSON.parse(localStorage.getItem("NOVO_USER"));

// Is logged in
export const isAuth = () => {
	const token = localStorage.getItem("token");
	return token ? token : null;
};

export const clearLS = () => {
	localStorage.clear();
};
